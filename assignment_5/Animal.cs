﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment_5
{
    public enum Gender
    {
        Male,
        Female
    }

    class Animal
    {
        // variable declarations  
        private string name;
        private int age;
        private double weight;
       
        public Gender Gender { get; set; }
      
        // properties 
        public string Name { get; set; }

        public int Age { get; set;  }
        public double Weight { get; set;}

        // constructors 
        public Animal()
        {

        }

        public Animal(int age)
        {
            this.Age = age; 
        }

        public Animal(string name, int age, double weight)
        {
            Name = name;
            Age = age;
            Weight = weight;
            Gender = Gender.Male;
        }

        public Animal(string name, int age, double weight, Gender gender)
        {
            Name = name;
            Age = age;
            Weight = weight;
            Gender = gender;
        }

        // behaviours (methods) 
        public void Hop(double distanceM)
        {
            Console.WriteLine($"{Name} can hop {distanceM} m away\n");
        }

      
        public void MakeNoise(string sound)
        {
            Console.WriteLine($"{Name} makes a sound: {sound}\n");
        }

        public void Sleep()
        {
            Console.WriteLine($"{Name} has gone to bed. Zzzzzz\n");
        }

        public override string ToString()
        {
            return $"{Name} is {Age} years old and weigh {Weight}kg it is a {Gender}\n";
        }



    }
}
