﻿using System;
using System.Collections.Generic; 

namespace assignment_5
{
    class Program
    {
        static void Main(string[] args)
        {
            // create muliple animal objects and assign appropriate properties
            Animal tiger = new Animal("Sheer Khan", 5, 300.0, Gender.Male);
            Animal lion = new Animal("Simba", 6, 180.0, Gender.Male);
            Animal wolf = new Animal();

            wolf.Name = "Destiny";
            wolf.Age = 7;
            wolf.Weight = 55.0; 
            wolf.Gender = Gender.Female;

            //  call functions 
            // hop functions
            tiger.Hop(8.0);
            lion.Hop(11.0);
            wolf.Hop(3.6);


            // makeNoise functions 

            tiger.MakeNoise("grrr, raaa");
            lion.MakeNoise("grauuw");
            wolf.MakeNoise(" ou ou ouooooo");

            // store them in a collection 
            List<Animal> animals = new List<Animal>
            {
                tiger,
                lion,
                wolf
            };
            foreach (Animal animal in animals)
            {
                Console.WriteLine(animal.ToString());
                 animal.Sleep();
              
            }
           

        }
    }
}
